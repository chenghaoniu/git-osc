---
layout: post
title: "ED25519-公钥支持"
---

- 新增企业版个人版对比页面 https://gitee.com/enterprise_contrast
- 添加 ED25519 公钥支持
- 企业工作台PR详情添加支持显示图片文件对比
- 修复偶尔Zip下载找不到文件的问题
- 修复企业版通过Commit无法关闭任务的问题
- 如果存在PR则禁止项目强制同步,防止强制同步导致PR差异不正确
- 修复PR回退导致的 Commits 列表显示错误
