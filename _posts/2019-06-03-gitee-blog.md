---
layout: post
title: "坑爹代码 | 循环+条件判断，你最多能嵌套几层？"
---

<p>for 循环和 if 条件判断语句，必不可少吧。但是你见过最多嵌套的循环和条件判断有几层呢？或者说，你最多能容忍多少层的嵌套呢？</p>

<p><img alt="" height="272" src="https://static.oschina.net/uploads/space/2019/0603/072147_Wj7H_12.jpg" width="279" /></p>

<p>我们还是先来看看极端的坑爹代码吧：</p>

<pre>
<code class="language-java">// 这个无限循环嵌套，只是总循环的一部分。。。我已经绕晕在黄桷湾立交
if (recordList.size() &gt; start) {
    for (int i = start; i &lt; end; i++) {
        Map&lt;String, Object&gt; map = recordList.get(i);
        Map&lt;String, Object&gt; field11 = (Map&lt;String, Object&gt;) map.get("field"); //name -&gt; code
        Map&lt;String, Object&gt; record11 = (Map&lt;String, Object&gt;) map.get("record"); // code -&gt; value
        String catagory1 = map.get("categoryId").toString();
        //  查询第一种类型对应的其他类型
        SalaryDataVo ss = JSON.parseObject(JSON.toJSONString(map), SalaryDataVo.class);
        Page page3 = salaryManagerService.getAllRecordsByCondition(ss);
        if (page3.getRecords().size() &gt; 0) {
            List&lt;Map&lt;String, Object&gt;&gt; salaryDataVos = page3.getRecords();
            salaryDataVos = this.reSetMap(salaryDataVos,null,null);
            for (Map&lt;String, Object&gt; map2 : salaryDataVos) {
                Map&lt;String, Object&gt; field2 = (Map&lt;String, Object&gt;) map2.get("field");
                Map&lt;String, Object&gt; record2 = (Map&lt;String, Object&gt;) map2.get("record");
                String catagory2 = map2.get("categoryId").toString();
                List&lt;SalaryGroupVO&gt; groupList2 = salaryGroupService.getSalaryGroupsItems(this.getUserCorpId(), catagory2);
                for (SalaryGroupVO cc : groupList2) {
                    cc.setCode(cc.getParentId() + cc.getCode());
                }
                //计算
                for (Map.Entry&lt;String, Object&gt; entity : field2.entrySet()) {
                    String keyName = entity.getKey();
                    for (SalaryGroupVO s2 : groupList2) {
                        if ("bigDecimal".equals(s2.getItemType()) &amp;&amp; s2.getCode().equals(field2.get(keyName).toString()) &amp;&amp; ("部门" != keyName) &amp;&amp; ("姓名" != keyName) &amp;&amp; StringUtils.isNotEmpty(s2.getItemType())) {
                            if (field11.containsKey(keyName)) {
                                if (field11.containsKey(keyName)) {
                                    String code1 = field11.get(keyName).toString();
                                    Double newValue = 0.0;
                                    Double oldValue=0.0;
                                    if (!record11.get(code1).toString().matches("^[0-9]*$")) {
                                        oldValue = Double.parseDouble(record11.get(code1).toString());
                                        if (record2.containsKey(entity.getValue().toString()) &amp;&amp; (!record2.get(entity.getValue().toString()).toString().matches("^[0-9]*$"))) {
                                            String value2 = record2.get(entity.getValue().toString()).toString();
                                            newValue = Double.parseDouble(value2);
                                        }
                                        record11.remove(field11.get(keyName).toString());
                                    }</code></pre>

<p>我数了数，一共有 11 层的嵌套！！！</p>

<p>吐槽归吐槽，这样的代码逻辑有什么重构的好方法吗？</p>

<p>到下面链接发表评论，领取奖品：</p>

<p><a href="https://gitee.com/oschina/bullshit-codes/blob/master/java/InfiniteCycle">https://gitee.com/oschina/bullshit-codes/blob/master/java/InfiniteCycle</a></p>

<p>码云 6 周年，我们正在征集各种坑爹代码，很多奖品等你来拿</p>

<p>详细的参与方法请看&nbsp;&nbsp;<a href="https://gitee.com/oschina/bullshit-codes">https://gitee.com/oschina/bullshit-codes</a></p>

<p>前两期坑爹代码：</p>

<ul>
	<li><a href="https://www.oschina.net/news/107032/gitee-bullshit-codes-stringbuffer" target="_blank">坑爹代码&nbsp;| 这样使用 StringBuffer 的方法有什么坑？</a></li>
	<li><a href="https://www.oschina.net/news/107081/gitee-bad-code-one-line-code" target="_blank">坑爹代码&nbsp;| 你写过的最长的一行代码有多长？？？</a></li>
</ul>
